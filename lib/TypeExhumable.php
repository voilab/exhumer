<?php
namespace Voilab\Exhumer;


interface TypeExhumable {

    /**
     * Méthode pour convertir une valeur extraite de la BD en une propriété typée
     *
     * @param string $value
     * @return mixed
     */
    public function unbury($value);

    /**
     * Méthode pour convertir une propriété typée en une string prête à être persistée en base
     *
     * @param string $value
     * @return mixed
     */
    public function bury($value);
} 