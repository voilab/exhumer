<?php
namespace Voilab\Exhumer;

class Exhumer
{
    /**
     *  Réfléction de la classe de base
     *
     *  @var \ReflectionClass
     */
    private $reflectionClass = null;

    /**
     *  Nom de la classe parent s'il y en a une
     *
     *  @var String
     */
    private $parentName = null;

    /**
     * Nom de la relation vers la class parent
     *
     * @example User
     * @var String
     */
    private $parentRelation = null;

    /**
     * Nom de la propriété qui contient le lien vers la class parent
     *
     * @example user_id
     * @var String
     */
    private $parentField = null;

    /**
     *  Liste des propriétés de la class de base
     *
     *  @var array of ReflectionProperty
     */
    private $properties = array();

    /**
     *  Conteneur Pimple, pour l'accès aux autres exhumers
     *
     *  @var \Pimple
     */
    private $container = null;

    /**
     * L'instance du modèle en train d'être déterré
     *
     * @var mixed
     */
    private $modelInstance = null;

    /**
     * Stockage des modèles déjà enterrés, afin de ne pas risquer de les enterrer récursivement
     *
     * @var array
     */
    private $buriedAlready = [];
    private $undefinedCounter = 0;

    /**
     *  Parse des block d'annotations dans les commentaires et retour dans un tableau PHP.
     *
     *  Exemples:
     *  @DiscriminatorColumn (name="discr", type="string")
     *  'DiscriminatorColumn' =>
     *      array (size=2)
     *        'name' => string 'discr' (length=5)
     *        'type' => string 'string' (length=6)
     *
     *  @Collection(mode="mangetasoupe\User")
     *  'Collection' =>
     *      array (size=1)
     *        'mode' => string 'mangetasoupe\User' (length=17)
     *
     *  @Wow Such annotation!
     *  'Wow' => string 'Such annotation!' (length=16)
     *
     *  @SoDynamic
     *  'SoDynamic' => null
     *
     *  @param  [type] $docComment [description]
     *
     *  @return [type]             [description]
     */
    static private function parseDocComment($docComment) {
        preg_match_all('/@([a-z0-9_-]+)[ \t]*(?:\((.+)\))?(.+)?/i', $docComment, $notes, PREG_SET_ORDER);
        $annotations = array();

        foreach ($notes as $note) {
            $value = null;

            if (isset($note[2])) {
                preg_match_all('/([^=]+)="([^"]+)",? */i', $note[2], $props, PREG_SET_ORDER);
                $value = array();

                foreach ($props as $prop) {
                    $value[$prop[1]] = $prop[2];
                }
            }

            if (isset($note[3]) && trim($note[3])) {
                $value = trim($note[3]);
            }

            $annotations[$note[1]] = $value;
        }

        return $annotations;
    }

    /**
     * Définition de la classe de base pour la création des objets.
     *
     * @param string $baseClass Nom de la classe de base
     * @return Exhumer
     */
    private function setBaseClass($baseClass) {
        $this->reflectionClass = new \ReflectionClass($baseClass);

        $properties = $this->reflectionClass->getProperties();

        // on parcoure les propriétés de la classe
        foreach ($properties as $prop) {
            $prop->setAccessible(true);

            // y a-t-il des annotations sur cette propriété ?
            $annotations = self::parseDocComment($prop->getDocComment());

            // définition des informations utile sur cette propriété
            $config = array(
                'ReflectionProperty' => $prop,
                'DeclaringClassName' => $prop->getDeclaringClass()->getName(),
                'isCollection' => isset($annotations['Collection']) && isset($annotations['Collection']['model']),
                'isModel' => isset($annotations['Model']) && isset($annotations['Model']['name']),
                'Type' => isset($annotations['Type']) ? $annotations['Type'] : 'DefaultType'
            );


            // si la propriété est une collection, on vérifie s'il y a un chemin
            // particulier dans le graphe d'objet pour aller récupérer les objets de ladite collection.
            if ($config['isCollection']) {
                $config['Model'] = $annotations['Collection']['model'];
                if (isset($annotations['Collection']['path'])) {
                    $config['dataPath'] = explode('.', $annotations['Collection']['path']);
                    $config['dataSource'] = array_shift($config['dataPath']);
                }
            }

            // si la propriété est un modèle, on ajoute les infos idoines dans notre petite $config de propriété
            // on vérifie également si l'annotation contient bien un lien clair vers une propriété de l'objet.
            if ($config['isModel']) {
                $config['Model'] = $annotations['Model']['name'];

                if (isset($annotations['Model']['property'])) {
                    $config['linkedProperty'] = $annotations['Model']['property'];
                } else {
                    trigger_error(sprintf("No 'property' option defined in %s property of %s class. This property will be ignored by Exhumer.", $prop->getName(), $prop->getDeclaringClass()->getName()), E_USER_WARNING);
                    continue;
                }
            }

            // enregistrement de cette config dans un tableau de propriétés gérées par l'exhumer.
            $this->properties[$prop->getName()] = $config;
        }

        // si la classe concernée dérive d'une autre classe, on va définir quelques infos supplémentaire sur la classe parente
        $parent = $this->reflectionClass->getParentClass();
        if ($parent) {
            $this->parentName = $parent->getName();
            $this->parentRelation = $parent->getShortName();
            $this->parentField = strtolower($parent->getShortName()) . '_id';
        }

        return $this;
    }

    /**
     *  Valeur d'une propriété pour cet objet. Gère les relations s'il y en a.
     *
     *  @param  String $propertyName  Nom de la propriété
     *  @param  Mixed  $propertyValue Valeur de la propriété
     *
     *  @return Mixed                 Valeur finale
     */
    private function getPropertyValue($propertyName, $propertyValue) {
        $config = $this->properties[$propertyName];

        if (isset($config['Model']) && $config['Model']) {
            $exhumer = $this->container[$config['Model']];

            // Collection d'objets
            if ($config['isCollection']) {
                $objects = array();

                foreach ($propertyValue as $object) {
                    if (isset($config['dataPath'])) {
                        foreach ($config['dataPath'] as $path) {
                            if (!isset($object[$path])) {
                                $object = null;
                                break;
                            }

                            $object = $object[$path];
                        }
                    }

                    if ($object !== null) {
                        $objects[] = $exhumer->unbury($object, $config['DeclaringClassName'], $this->modelInstance);
                    }
                }

                return $objects;
            }

            // Model simple
            if ($config['isModel']) {
                if ($propertyValue) {
                    return $exhumer->unbury($propertyValue);
                } else {
                    return null;
                }
            }
        }

        // Gestion du type de la propriété
        if (isset($this->container['type'][$config['Type']])) {
            return $this->container['type'][$config['Type']]->unbury($propertyValue);
        }

        return $propertyValue;
    }

    /**
     * Défini les propriétés d'un objet
     *
     * @param object $object Objet cible
     * @param array $data Données
     * @param bool $isParent Est-on en train de remplir les propriétés de la table parente
     * @return object Un objet instancié
     */
    private function setProperties($object, $data, $isParent = false) {
        foreach ($this->properties as $name => $prop) {
            if (isset($data[$name]) && (!$isParent || $object->$name === null)) {
                $prop['ReflectionProperty']->setValue(
                    $object,
                    $this->getPropertyValue($name, $data[$name])
                );
            }
            if (isset($prop['dataSource']) && isset($data[$prop['dataSource']])) {
                $prop['ReflectionProperty']->setValue(
                    $object,
                    $this->getPropertyValue($name, $data[$prop['dataSource']])
                );
            }
        }

        if ($this->parentRelation && isset($data[$this->parentRelation])) {
            $object = $this->setProperties($object, $data[$this->parentRelation], true);
        }

        return $object;
    }

    /**
     * Création d'un Exhumer pour une classe.
     *
     * @param string $baseClass Nom de la classe
     * @param Container $container Le container de dépendance de l'Exhumer
     */
    public function __construct($baseClass, Container $container) {
        $this->setBaseClass($baseClass);
        $this->container = $container;
    }

    /**
     *  Extraction des données d'un objet
     *
     *  @param  object $object Objet de la class $this->reflectionClass
     *
     *  @return array          Données
     */
    public function bury($object) {
        if (!$object || !$this->reflectionClass->isInstance($object)) {
            return false;
        }

        // si on souhaite prévenir le risque de récursion infinie lié à l'envoi au trou d'un
        // objet qui contient des relations circulaires, on peut activer la config ci-dessous.
        // Mais ça implique que tous les modèles enterrés possèdent un champ 'id', sinon on sait pas
        // trop ce qui se passera...
        if ($this->container['config']['prevent_recursive_bury']) {
            $object_identifier = get_class($object) . '_' . ($object->id ?: 'undefined' . ++ $this->undefinedCounter);
            if (in_array($object_identifier, $this->buriedAlready)) {
                return null;
            }
            $this->buriedAlready[] = $object_identifier;
        }

        $data = array();

        foreach ($this->properties as $name => $prop) {
            if (!$prop['ReflectionProperty']->isPublic()) {
                continue;
            }

            $propertyValue = $prop['ReflectionProperty']->getValue($object);

            if (isset($prop['Model']) && $prop['Model']) {
                $exhumer = $this->container[$prop['Model']];

                if ($prop['isCollection'] && is_array($propertyValue)) {
                    $values = array();

                    foreach ($propertyValue as $sobject) {
                        $values[] = $exhumer->bury($sobject);
                    }

                    $propertyValue = $values;
                }

                if ($prop['isModel']) {
                    $propertyValue = $exhumer->bury($propertyValue);
                    if (isset($propertyValue['id']) && $propertyValue['id']) {
                        $data[$prop['linkedProperty']] = $propertyValue['id'];
                    }
                }
            } else {
                // Gestion du type de la propriété
                if (isset($this->container['type'][$prop['Type']])) {
                    $propertyValue = $this->container['type'][$prop['Type']]->bury($propertyValue);
                }
            }

            if ($prop['DeclaringClassName'] === $this->parentName) {
                $data[$this->parentRelation][$name] = $propertyValue;
            } else {
                $data[$name] = $propertyValue;
            }
        }

        if ($this->parentRelation && (isset($data[$this->parentField]) || $data[$this->parentField] === null)) {
            $data[$this->parentRelation]['id'] = $data[$this->parentField];
            unset($data[$this->parentField]);
        }

        return $data;
    }

    /**
     *  Création d'une nouvelle instance d'un objet et set des données en paramètres
     *
     *  @param  array  $data Données à injecter dans l'objet
     *  @param  array  $args Paramètres à passer au constructeur
     *
     *  @return object       Instance
     */
    public function create($data, $args = array()) {
        $object = $this->reflectionClass->newInstanceArgs($args);

        return $this->setProperties($object, $data);
    }

    /**
     *  Instanciation d'un objet avec les données passées en paramètres
     *
     *  @param  array $data Données à injecter dans l'objet
     *
     *  @return object      Instance
     */
    public function unbury($data, $parentRelationModel = null, $parentRelationInstance = null) {
        $this->modelInstance = $this->reflectionClass->newInstanceWithoutConstructor();

        $this->setProperties($this->modelInstance, $data);

        // si une relation parente (on parle de relation et pas d'héritage) a été fournie, on peut la réinjecter en référence
        // dans cette instance, ainsi elle a accès au modèle qui l'a instanciée, elle.
        if ($parentRelationModel && $parentRelationInstance) {
            $link_field = substr(strrchr($parentRelationModel, "\\"), 1); // @todo: trouver un meilleur moyen d'identifier la propriété de lien
            $this->modelInstance->$link_field = $parentRelationInstance;

        }
        return $this->modelInstance;
    }

    /**
     *  Unbury d'une collection de données. Retourne une liste d'objets
     *
     *  @param  array  $objects Données
     *
     *  @return array           Objets
     */
    public function unburyCollection(array $objects) {
        $result = array();

        foreach ($objects as $object) {
            $result[] = $this->unbury($object);
        }

        return $result;
    }
}
