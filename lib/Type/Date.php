<?php
namespace Voilab\Exhumer\Type;


use Voilab\Exhumer\TypeExhumable;

class Date implements TypeExhumable {

    /**
     * Méthode pour convertir une valeur extraite de la BD en une propriété typée
     *
     * @param string $value
     * @return \DateTime
     */
    public function unbury($value) {
        if ($value && $value != '0000-00-00') {
            return new \DateTime($value);
        }

        return null;
    }

    /**
     * Méthode pour convertir une propriété typée en une string prête à être persistée en base
     *
     * @param \DateTime $value
     * @return string
     */
    public function bury($value) {
        if ($value instanceof \DateTime) {
            return $value->format('Y-m-d');
        }

        return null;
    }
}