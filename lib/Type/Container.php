<?php

namespace Voilab\Exhumer\Type;

class Container extends \Pimple\Container {

    public function __construct()
    {
        parent::__construct();

        $this['Date'] = function($c) {
            return new Date();
        };

        $this['Time'] = function($c) {
            return new Time();
        };

        $this['DateTime'] = function($c) {
            return new DateTime();
        };

        $this['Boolean'] = function($c) {
            return new Boolean();
        };

        $this['Integer'] = function($c) {
            return new Integer();
        };
    }

}