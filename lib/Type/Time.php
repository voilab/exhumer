<?php
namespace Voilab\Exhumer\Type;


use Voilab\Exhumer\TypeExhumable;

class Time implements TypeExhumable {

    /**
     * Méthode pour convertir une valeur extraite de la BD en une propriété typée
     *
     * @param string $value
     * @return \DateTime
     */
    public function unbury($value) {
        return new \DateTime('1970-01-01 ' . $value);
    }

    /**
     * Méthode pour convertir une propriété typée en une string prête à être persistée en base
     *
     * @param \DateTime $value
     * @return string
     */
    public function bury($value) {
        if ($value instanceof \DateTime) {
            return $value->format('H:i:s');
        }

        return null;
    }
}