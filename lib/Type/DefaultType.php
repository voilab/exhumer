<?php
namespace Voilab\Exhumer\Type;


use Voilab\Exhumer\TypeExhumable;

class DefaultType implements TypeExhumable {

    /**
     * Méthode pour convertir une valeur extraite de la BD en une propriété typée
     *
     * @param string $value
     * @return bool
     */
    public function unbury($value) {
        return $value;
    }

    /**
     * Méthode pour convertir une propriété typée en une string prête à être persistée en base
     *
     * @param bool $value
     * @return mixed
     */
    public function bury($value) {
        return $value;
    }
}