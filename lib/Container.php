<?php

namespace Voilab\Exhumer;

class Container extends \Pimple\Container {

	public function __construct(array $config)
	{
		parent::__construct();

		$this['config'] = array_merge([
			'prevent_recursive_bury' => false
		], $config);

        $this['type'] = function () {
            return new \Voilab\Exhumer\Type\Container();
        };
	}
}