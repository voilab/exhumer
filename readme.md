# PHP Exhumer

Small models unserializer tool.
Takes an array of data and create a class instance based on that data.

## How to Install

#### using [Composer](http://getcomposer.org/)

Create a composer.json file in your project root:

```json
{
    "require": {
        "voilab/exhumer": "0.2.*"
    }
}
```

Then run the following composer command:

```bash
$ php composer.phar install
```

#### Usage

Exemple class
```php
class User {
    private $name = '';
    private $password = '';
    private $value = 0;

    public function getValue() {
        return $this->value + 1;
    }
}
```

Instantiate an Exhumer for a class
```php
$exhumer = new \Voilab\Exhumer\Exhumer('User');
```

### Unbury
```php
$object = $exhumer->unbury(
    array(
        'name' => 'username',
        'password' => 'password123',
        'value' => 41,
        'dummy' => 'test'
    )
);
```

Note that `dummy` is ignored since it's not present in the base class.
```php
object(User)[6]
  private 'name' => string 'username' (length=8)
  private 'password' => string 'password123' (length=11)
  private 'value' => int 41
```

Will return `42`
```php
$object->getValue();
```

### Bury
```php
$data = $exhumer->bury($object);
```

```php
array (size=3)
  'name' => string 'username' (length=8)
  'password' => string 'password123' (length=11)
  'value' => int 41
```

## Authors

[Alexandre Ravey](http://www.voilab.org)

## License

MIT Public License
